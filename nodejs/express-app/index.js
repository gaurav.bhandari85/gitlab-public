var express = require('express');
var app = express();
var fs = require("fs");
const pug = require('pug');

// Set View Engine to Pug
app.set("view engine", "pug");

const json2html = require('node-json2html');

app.get('/', function (req, res) {
  res.send('Welcome to Express!');
});


// Query Portfolio
app.get('/portfolio', function (req, res) {
    var content = fs.readFileSync("response.json");
    var jsonContent = JSON.parse(content);

    let template = {'<>':'div','html':'${responseId} ${year}'};
    let html = json2html.transform(jsonContent,template);

    var load="";
    for(var exKey in jsonContent) {
        console.log("key:"+exKey+", value:"+jsonContent[exKey]);
        load = load + jsonContent[exKey]+",";

    }

    // Return the name of template in the views folder
    res.render("template",
    {
      name: 'Gaurav Bhandari'
    }
    );

 })


var server = app.listen(8000, function () {
  var host = 'localhost';
  var port = server.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});