# Express with Nodejs

## Install the following project

```
npm install
```

## Start the Project

```
node index.js
```

## Accessible URL's
http://localhost:8000/
and 
http://localhost:8000/portfolio
